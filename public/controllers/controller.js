function AppCtrl($scope, $http) {
	console.log("Hello world from controller");

	var refresh = function() {
		$http.get('/contactlist').success(function(response){
			console.log("I got the data I requested");
			$scope.contactlist = response;
			$scope.contact = "";
		});
	};

	refresh();

	$scope.addContact = function(){
		var bolName = true;
		var bolData = true;
		if ($scope.contact.name == undefined || $scope.contact.name == ""){
			console.log("Error- No Name");
			bolName = false;
		}
		if ((($scope.contact.email == undefined) && ($scope.contact.number == undefined)) || (($scope.contact.email == "") && ($scope.contact.number == "")) ){
			console.log("Error- Gotta have an email or a number at least")
			bolData = false;
		}
		if(bolName == true && bolData == true){
		console.log($scope.contact.name);
		$http.post('/contactlist', $scope.contact).success
		(function(response){
			console.log(response);
			refresh();
		});
		}
	};

	$scope.remove = function(id){
		console.log(id);
		$http.delete('/contactlist/' + id).success(function (response){
			refresh();
		});
	};

	$scope.edit = function(id){
		console.log(id);
		$http.get('/contactlist/' + id).success(function (response){
			$scope.contact = response;
			
		});
	};

	$scope.update = function() {
		console.log($scope.contact._id);
		$http.put('/contactlist/' + $scope.contact._id, $scope.contact).success(function (response){
			refresh();
		})
	};

	$scope.deselect = function() {
		$scope.contact = "";
	};


}